import Vue from 'vue'

//Main pages
import * as VueGoogleMaps from 'vue2-google-maps'
import App from './views/app.vue'

Vue.config.productionTip = false
// const app = new Vue({
//     el: '#app',
//     components: { App }
// });

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDPSp37X3waDDvB0X6-GXHOJdnREHsJHyY',
        libraries: 'places',
    }
});

new Vue({
    render: h => h(App),
}).$mount('#app')
